# TTi_Fan_Control

This is a very basic script for controlling the voltage of a TTiCPX400SP power supply. 
This is originally intended to be used to send commands to the power supply
controlling the fans inside the testbeam coldbox, so it is nothing fancy. 

## Getting started

This repo requires the following python package(s):

  * serial

You should already have these installed, so no need to do anything.

## How to Operate

Go to the directory containing the repository, currently, navigate to
the following directory on the PC in the DESY testbeam hall:

```bash
cd /home/itkstrip/TB2211/tti_fan_control/
```

Once there, open up python3

```bash
python3
```

Import the package and create the object, I'm using 'fan' since it's easy:

```python
import TTiCPX400SP as TTi
fan = TTi.TTiCPX400SP()
```

The port being used is hard-coded. Not my favorite way to do things, but 
as long as you keep using this particular supply, you won't have any issues.
If you change power supplies, you can find the correct port in
`/dev/serial/by-id/`.

## Basic Functions

I am only allowing the user to read and control the voltage, as well as read
the current (if you're bored or whatever). After performing the steps above
(you will need to do those any time you exit the python CLI), do the following:

### Read Voltage

```python
fan.get_voltage()
```

Which will give you an output:

```bash
TTi voltage is 8.489 V
```

This voltage of course will not exactly match the display, but it's the 'right' value.

### Read Current

```python
fan.get_current()
```

Which will give you the output:

```bash
TTi Current is 0.172 A
```

Same argument as above.

### Set Voltage

This is the one you're likely to use.

```python
fan.set_voltage(8.50)
```

Which will let you know the value you've set it to:

```bash
Setting TTi voltage to 8.50 V
```

There is a setting in the TTiCPX400SP.py script, called `self._max_volt`, located in the 
`__init__`. It is 24V at the moment, and you may choose to change it. If you try to set
the voltage above this max setting, you'll get a funny message an nothing will happen.
