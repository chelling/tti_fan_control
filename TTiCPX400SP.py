import serial

class TTiCPX400SP():
  
  def __init__(self, timeout = 2):
    '''
    Initialize TTiCPX400SP class
    '''
    # This is the serial address to this specific TTi and must
    # be changed if using a different device.
    port = '/dev/serial/by-id/usb-TTi_CPX_Series_PSU_570311-if00'
    
    # This code was originally written for the TTiCPX400DP, which has
    # more than one channel. To keep from a bunch of rewriting, we'll
    # just assign channel 1 here.
    self._channel = 1
    # Max voltage for safety (change it if you know it should be otherwise)
    self._max_volt = 10
    # Set up serial connection
    try:
      self.ser = serial.Serial(port, timeout = timeout, write_timeout = timeout)
    except:
      print(f'Unable to establish a connection for port {port}. Are you sure it exists?')
      print('Check in the directory /dev/serial/by-id/')
  
  def _send_command(self, command):
    '''
    Send command to power supply
      Adds \n termination character
    '''
    
    command += '\n'
    self.ser.write(command.encode())
  
  def _receive(self):
    '''
    Read from serial connection and decode
    '''
    
    ret = self.ser.read_until()
    return ret.decode()
  
  def enable_output(self):
    '''
    Enable output for channel
    '''
    
    query = f'OP{self._channel} 1'
    self._send_command(query)
  
  def disable_output(self):
    '''
    Disable output for channel
    '''
    self.set_voltage(0)
    
    query = f'OP{self._channel} 0'
    self._send_command(query)
  
  def get_voltage(self):
    '''
    Get voltage reading for channel
    '''
    
    query = f'V{self._channel}O?'
    self._send_command(query)
    reply = self._receive()
    voltage = float(reply[:-3])
    print(f'TTi voltage is {voltage} V')
  
  def set_voltage(self, voltage):
    '''
    Set voltage for channel
    '''
    
    if (voltage > self._max_volt or voltage < 0):
      print(f'{voltage}V .... Are you high?') 
      print(f'Try again with something below {self._max_volt} V')
      print('Make it reasonable please :).')
      return

    print(f'Setting TTi voltage to {voltage} V')
    query = f'V{self._channel} {voltage}'
    self._send_command(query)
  
  def get_current(self):
    '''
    Get current reading for channel
    '''
    
    query = f'I{self._channel}O?'
    self._send_command(query)
    reply = self._receive()
    current = float(reply[:-3])
    print(f'TTi Current is {current} A')

